import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutes } from './app.route';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { LoginComponent } from '../app/login/login.component';
import { MainPageComponent } from '../app/main-page/main-page.component';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { HeaderComponent } from '../app/header/header.component';
import { MainBodyComponent } from '../app/main-body/main-body.component';
import { AboutComponent } from '../app/about/about.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainPageComponent,
    DashboardComponent,
    HeaderComponent,
    MainBodyComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    FormsModule
  ],
  exports: [BsDropdownModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
