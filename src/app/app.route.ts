import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// import {AppComponent} from './app.component';
import {LoginComponent} from '../app/login/login.component';
import { MainPageComponent } from '../app/main-page/main-page.component';
import {DashboardComponent} from '../app/dashboard/dashboard.component';
import { AboutComponent } from '../app/about/about.component';

export const AppRoutes: Routes = [{
  path: '',
  component: LoginComponent
},
{
    path: 'index',
    component: MainPageComponent,
    children: [
         { path: '', component: DashboardComponent },
         { path: 'about', component: AboutComponent }
      ]
  }];

export const routers: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
